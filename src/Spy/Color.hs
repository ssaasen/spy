module Spy.Color (
    green
  , red
  , yellow
  , Color(..)
  , Mode(..)
  , bold
  , ansi
) where

data Color = Black | Blue | Green | Cyan | Purple | Red | Brown |
             LightGray | DarkGray | LightBlue | LightGreen | LightCyan |
             LightRed | LightPurple | Yellow | White deriving (Show, Eq, Ord)

data Mode = Foreground | Background deriving (Show, Eq, Ord)

-- | Return the String wrapped with ANSI escape codes to turn the
-- foreground red
--
-- >>> red "test"
-- "\ESC[0;31mtest\ESC[0m"
green, red, yellow :: String -> String
green = ansi Foreground Green
red = ansi Foreground Red
yellow = ansi Foreground Yellow


-- | Return the given String with ANSI escape codes for bold
-- >>> bold "test"
-- "\ESC[1mtest\ESC[0m"
bold :: String -> String
bold s = "\x1b[1m" ++ s ++ "\x1b[0m"

ansi :: Mode -> Color -> String -> String
ansi m c s = "\x1b["++ toCode m c ++ "m" ++ s ++ "\x1b[0m"



-- |
-- >>> toCode Foreground Green
-- "0;32"
--
-- >>> toCode Background LightPurple
-- "1;45"
toCode :: Mode -> Color -> String
toCode m Black        = asAnsi m 0 0
toCode m Blue         = asAnsi m 0 4
toCode m Green        = asAnsi m 0 2
toCode m Cyan         = asAnsi m 0 6
toCode m Purple       = asAnsi m 0 5
toCode m Red          = asAnsi m 0 1
toCode m Brown        = asAnsi m 0 3
toCode m LightGray    = asAnsi m 0 7
toCode m DarkGray     = asAnsi m 1 0
toCode m LightBlue    = asAnsi m 1 4
toCode m LightGreen   = asAnsi m 1 2
toCode m LightCyan    = asAnsi m 1 6
toCode m LightRed     = asAnsi m 1 1
toCode m LightPurple  = asAnsi m 1 5
toCode m Yellow       = asAnsi m 1 3
toCode m White        = asAnsi m 1 7

asAnsi :: Mode -> Int -> Int -> String
asAnsi Foreground = foreground
asAnsi Background = background

background, foreground :: Int -> Int -> String
foreground l r = show l ++ ";3" ++ show r

background l r = show l ++ ";4" ++ show r

