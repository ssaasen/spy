require 'rubygems'
require 'sysinfo'
require 'fileutils'

ARTIFACT=Rake::FileList[".stack-work/dist/**/build/spy/spy"].first
DISTRIBUTION="./distribution".freeze

desc "Clean build artifacts"
task :clean do
  stack 'clean'
end

desc "Create the spy binary"
task :build => [:clean, :compile]

task :compile do
  stack 'build'
end


desc "Run the doctest tool on all Haskell files in src/"
task :doctest do
  `doctest ./src/**/*.hs`
end

desc "Build and package the spy distribution"
task :package => [:build, :generate_man, :contributors] do
  include FileUtils
  with_temp_dir "spy" do |package_dir|
    cp "README.md", package_dir
    %w(LICENSE CONTRIBUTORS).each {|f| cp f, package_dir }
    bin_dir = File.join(package_dir, "bin")
    mkdir bin_dir
    cp ARTIFACT, bin_dir
    cp_r "#{DISTRIBUTION}/man", package_dir
    cp_r "#{DISTRIBUTION}/Makefile", package_dir
    sysinfo = SysInfo.new
    `tar zfc spy-#{sysinfo.impl}-#{sysinfo.arch}-#{version}.tar.gz spy`
  end
end

desc "Run Hlint on the sources"
task :hlint => :build do
  print `hlint src -c --report --utf8`
end

desc "Rebuild and copy spy into the stack bin directory"
task :rebuild => :build do
  stack 'install'
end

desc "Create man page"
task :generate_man do
  `pandoc -s -w man ./#{DISTRIBUTION}/man/spy.1.md -o ./#{DISTRIBUTION}/man/spy.1`
end

desc "Run the tests"
task :test do
  stack 'test'
end

desc "Update CONTRIBUTORS file"
task :contributors do
  File.open("CONTRIBUTORS", "w") do |f|
    `git shortlog -s -n`.split("\n").each do |line|
      n, name = line.split("\t")
      f << name
      f << "\n"
    end
  end
end

desc "Update version to the next minor version, tag the result"
task :bump_minor_version do
  current_version = `git describe --tags`.strip.split("-").first.gsub(/^v/, '')
  major, minor = current_version.split(".").map {|e| e.to_i}
  next_version = [major, minor.next].join(".")

  # Update version in Main
  update_file(File.join("src", "Main.hs"), /version = "[^"]+"/, "version = \"spy v#{next_version}, (C) Stefan Saasen\"")

  # Update cabal file
  update_file("spy.cabal", /^version:.*$/, "version:            #{next_version}")

  # Commit and tag
  msg = "Bump version to #{next_version}"
  puts msg
  `git add -u`
  `git commit -m "#{msg}"`
  `git tag -a -m "#{msg}" v#{next_version}`
end

def version
  `git describe --tag --always`.strip
end

def stack(cmd)
  print `stack #{cmd}`
  raise "Failed to execute 'stack #{cmd}'" unless $?.to_i.eql?(0)
end

def with_temp_dir(name)
  raise "The directory #{name} already exists" if File.directory?(name)
  mkdir name
  yield name
ensure
  rm_r name
end


def update_file(file, pattern, replace)
  updated = IO.read(file)
  File.open(file, "w") do |f|
    f << updated.gsub(pattern, replace)
  end
end

